function randomType() {
    return Math.random() >= 0.5 ? 'in' : 'out';
}

function randomMessage() {
    var text = 'Lorem ipsum ';
    var count = Math.floor(Math.random() * 20) + 1;
    var result = '';

    for(var i = 0; i < count; i++) {
        result += text;
    }

    return result;
}

export default function (state = [], action)  {
    var messages = [];

    for(var i = 1; i <= 50; i++) {
        messages.push({ 
            id: i,
            type: randomType(),
            text: randomMessage()
        });
    }

    return messages;
}