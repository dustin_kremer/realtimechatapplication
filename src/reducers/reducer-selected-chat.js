import { SELECT_CHAT } from '../actions/index';

export default function (state = null, action)  {
    switch (action.type) {
        case SELECT_CHAT:
            return action.payload;
        default:
            return state;
    }
}