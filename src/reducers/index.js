import { combineReducers } from 'redux';
import chatsReducer from './reducer-chats';
import selectedChatReducer from './reducer-selected-chat';
import messagesReducer from './reducer-messages';

export default combineReducers({
  chats: chatsReducer,
  selectedChat: selectedChatReducer,
  messages: messagesReducer
});
