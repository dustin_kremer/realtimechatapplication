import { LOAD_CHATS } from '../actions/index';

export default function (state = [], action)  {
    switch (action.type) {
        case LOAD_CHATS:
            return action.payload.data;
    }
    
    return state;
}