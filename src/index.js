import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import promiseMiddleware from 'redux-promise';
import injectTapEventPlugin from 'react-tap-event-plugin';

import App from './components/app';
import reducers from './reducers';

injectTapEventPlugin();

const providerStore = applyMiddleware(promiseMiddleware)(createStore)(reducers);
ReactDOM.render(<Provider store={providerStore}><App /></Provider>, document.querySelector('.container'));