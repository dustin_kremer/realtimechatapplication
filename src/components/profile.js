import React, { Component } from 'react';
import Avatar from 'material-ui/Avatar';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import ChatIcon from 'material-ui/svg-icons/communication/chat';

export default class Profile extends Component {
    render() {
        return (
            <div className="profile">
                <div className="avatar">
                    <Avatar src="" size={48} />
                </div>
                <div className="menu">
                    <IconMenu
                        iconButtonElement={<IconButton><ChatIcon /></IconButton>}
                        anchorOrigin={{horizontal: 'left', vertical: 'top'}}
                        targetOrigin={{horizontal: 'left', vertical: 'top'}}>
                        <MenuItem primaryText="Item 1" />
                        <MenuItem primaryText="Item 2" />
                    </IconMenu>
                </div>
                <div className="menu">
                    <IconMenu
                        iconButtonElement={<IconButton><MoreVertIcon /></IconButton>}
                        anchorOrigin={{horizontal: 'left', vertical: 'top'}}
                        targetOrigin={{horizontal: 'left', vertical: 'top'}}>
                        <MenuItem primaryText="Item 1" />
                        <MenuItem primaryText="Item 2" />
                    </IconMenu>
                </div>
            </div>
        );
    }
}