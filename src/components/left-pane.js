import React, { Component } from 'react';

import Profile from './profile';
import Search from './search';
import Chats from '../containers/chats';

export default class LeftPane extends Component {
    render() {
        return (
            <div className="left-pane">
                <Profile />
                <Search />
                <Chats />
            </div>
        );
    }
}
