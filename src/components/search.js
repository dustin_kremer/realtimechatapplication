import React, { Component } from 'react';
import TextField from 'material-ui/TextField';

export default class Search extends Component {
    onFormSubmit(event) {
        event.preventDefault();
    }

    render() {
        return (
            <form className="search" onSubmit={this.onFormSubmit.bind(this)}>
                <input placeholder="Search for chats or contacts" />
            </form>
        );
    }
}