import React, { Component } from 'react';
import Paper from 'material-ui/Paper';

import LeftPane from './left-pane';
import RightPane from './right-pane';

export default class Drawer extends Component {
    render() {
        return (
            <div className="drawer">
                <Paper className="paper" zDepth={1}>
                    <LeftPane />
                    <RightPane />
                </Paper>
            </div>
        );
    }
}
