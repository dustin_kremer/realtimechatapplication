import React, { Component } from 'react';

export default class Message extends Component {
    render() {
        return (
            <div className={`message type-${this.props.type}`}>{this.props.text}</div>
        );
    }
}