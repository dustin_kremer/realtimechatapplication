import React, { Component } from 'react';

import Intro from '../containers/intro';
import Conversation from '../containers/conversation';

export default class RightPane extends Component {
    render() {
        return (
            <div className="right-pane">
                <Intro />
                <Conversation />
            </div>
        );
    }
}
