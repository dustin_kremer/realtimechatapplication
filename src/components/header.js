import React, { Component } from 'react';
import Avatar from 'material-ui/Avatar';

export default class Header extends Component {
    render() {
        return (
            <div className="header">
                <div className="header-avatar">
                    <Avatar src={this.props.avatar} size={48} />
                </div>
                <div className="header-body">
                    <div className="header-main">
                        <div className="header-title">
                            <span>{this.props.title}</span>
                        </div>
                    </div>
                    <div className="header-secondary">
                        <div className="header-status">
                            <span>{this.props.status}</span>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}