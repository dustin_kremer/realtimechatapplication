import React, { Component } from 'react';
import Avatar from 'material-ui/Avatar';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { selectChat } from '../actions/index';

class Chat extends Component {
    render() {
        return (
            <div 
                className={`chat ${this.props.selectedChat && this.props.selectedChat.id == this.props.id ? "is-selected" : ""}`} 
                onClick={() => this.props.selectChat(this.props)}>
                <div className="chat-avatar">
                    <Avatar src={this.props.avatar} size={48} />
                </div>
                <div className="chat-body">
                    <div className="chat-main">
                        <div className="chat-title">
                            <span>{this.props.title}</span>
                        </div>
                        <div className="chat-meta">
                            <span>{this.props.meta}</span>
                        </div>
                    </div>
                    <div className="chat-secondary">
                        <div className="chat-status">
                            <span>{this.props.status}</span>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default connect(({ selectedChat }) => { return { selectedChat } }, { selectChat })(Chat);