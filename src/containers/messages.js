import React, { Component } from 'react';
import Avatar from 'material-ui/Avatar';
import { connect } from 'react-redux';

import Message from '../components/message';

class Messages extends Component {
    render() {
        return (
            <div className="messages">
                {this.props.messages.map(message => { return <Message key={message.id} {...message} /> })}
            </div>
        );
    }
}

export default connect(({ messages }) => { return { messages } })(Messages);