import React, { Component } from 'react';
import Avatar from 'material-ui/Avatar';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import Header from '../components/header';
import Messages from './messages';

class Chat extends Component {
    render() {
        if(!this.props.selectedChat) {
            return (<div></div>);
        }

        return (
            <div className="conversation">
                <Header {...this.props.selectedChat} />
                <Messages />
            </div>
        );
    }
}

export default connect(({ selectedChat }) => { return { selectedChat } })(Chat);