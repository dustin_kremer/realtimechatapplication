import React, { Component } from 'react';
import Avatar from 'material-ui/Avatar';
import { connect } from 'react-redux';

import { loadChats } from '../actions/index';
import Chat from './chat';

class Chats extends Component {
    componentWillMount() {
        this.props.loadChats();
    }
    
    render() {
        return (
            <div className="chats">
                {this.props.chats.map(chat => { return <Chat key={chat.id} {...chat} /> })}
            </div>
        );
    }
}

export default connect(({ chats }) => { return { chats } }, { loadChats })(Chats);