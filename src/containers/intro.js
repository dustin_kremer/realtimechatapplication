import React, { Component } from 'react';
import Avatar from 'material-ui/Avatar';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

class Intro extends Component {
    render() {
        if(this.props.selectedChat) {
            return <div></div>;
        }

        return <div className="intro">No active chat</div>;;
    }
}

export default connect(({ selectedChat }) => { return { selectedChat } })(Intro);