import axios from 'axios';

export const LOAD_CHATS = 'LOAD_CHATS';
export const SELECT_CHAT = 'SELECT_CHAT';

const baseUrl = "http://realtimechatapplicationapi-dkremer718112.codeanyapp.com:8080";
const chatController = `${baseUrl}/API/Chat`;

export function selectChat(chat) {
    return {
        type: SELECT_CHAT,
        payload: chat
    };
}

export function loadChats() {
    return {
        type: LOAD_CHATS,
        payload: axios(`${chatController}/GetAll`)
    };
}